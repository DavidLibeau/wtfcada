var fs = require('fs');
var util = require('util');
var log_file = fs.createWriteStream(__dirname + '/lastLogs.txt', {
    flags: 'w'
});
var log_stdout = process.stdout;

console.log = function (d) { //
    log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
};

var moment = require('moment');
require('moment/locale/fr.js');
moment().locale('fr');
const parse = require('csv-parse')
const assert = require('assert')

let filename = "cada-2021-06-08.csv"
let data = fs.readFileSync(process.cwd() + "/" + filename).toString()

let filenameReception = "cada-reception.csv"
let dataReception = fs.readFileSync(process.cwd() + "/" + filenameReception).toString()

var saisines = []
var saisinesAvecDelai = []
var durations = [];
var durationsPerYear = {};
var lengthPerYear = {};
var totalPerYear = {};
var statsError = 0;
var errorsPerYear = {};
var saisinesParAnneeParType = {};
var delaiParAnneeParType = {};
var csvDurationsPerYear = "";

var dateReception = {};
parse(dataReception, {
        trim: true,
        skip_empty_lines: true
    })
    .on('readable', function () {
        let record
        while (record = this.read()) {
            if (dateReception[record[0]]) {
                console.log("Duplicate in data reception : " + record[0]);
            }
            dateReception[record[0]] = record[1];
        }
    })
    .on('end', function () {
        console.log("Done parsing reception");
        parse(data, {
                trim: true,
                skip_empty_lines: true
            })
            .on('readable', function () {
                let record
                while (record = this.read()) {
                    saisines.push(record)
                }
            })
            .on('end', function () {
                console.log(saisines[0])
                for (var i = 1; i < saisines.length - 1; i++) {
                    if(!lengthPerYear[saisines[i][3]]){
                        lengthPerYear[saisines[i][3]] = [];
                    }
                    lengthPerYear[saisines[i][3]].push(saisines[i][10].length);
                    
                    var type = saisines[i][8].split(",")[0].split("/")[0];
                    if (type == '') {
                        type = 'Sans objet';
                    }
                    if (!saisinesParAnneeParType[saisines[i][3]]) {
                        saisinesParAnneeParType[saisines[i][3]] = {
                            "année": saisines[i][3],
                            'Favorable': 0,
                            'Défavorable': 0,
                            'Incompétence': 0,
                            'Irrecevable': 0,
                            'Sans objet': 0
                        };
                    }
                    saisinesParAnneeParType[saisines[i][3]][type]++;
                    if (!totalPerYear[saisines[i][3]]) {
                        totalPerYear[saisines[i][3]] = 0;
                    }
                    totalPerYear[saisines[i][3]]++;
                    /*if (datesExtract.length > 1) {
                    	console.log(datesExtract);
                    	console.log(saisines[i]);
                    }*/
                    var dateAvis = parseDate(saisines[i][4]);
                    if (dateReception[saisines[i][0]]) {
                        var duration = moment.duration(dateAvis.diff(dateReception[saisines[i][0]]));
                        if (duration.asDays() < 0) {
                            console.log(duration.asDays());
                            console.log(dateAvis);
                            console.log(dateReception[saisines[i][0]]);
                            console.log(saisines[i][0]);
                            console.log("Erreur : Avis " + saisines[i][0] + " rendu le " + saisines[i][4] + " alors que saisine le " + datesExtracted[0] + " (" + duration.asDays() + " jours) !");
                            incrementError(saisines[i][3]);
                        } else {
                            if (!durationsPerYear[saisines[i][3]]) {
                                durationsPerYear[saisines[i][3]] = [];
                            }
                            durationsPerYear[saisines[i][3]].push(duration.asDays())
                            durations.push(duration.asDays());

                            saisinesAvecDelai.push({
                                'Numéro de dossier': saisines[i][0],
                                'Administration': saisines[i][1],
                                'Type': saisines[i][2],
                                'Année': saisines[i][3],
                                'Séance': saisines[i][4],
                                'Objet': saisines[i][5],
                                'Thème et sous thème': saisines[i][6],
                                'Mots clés': saisines[i][7],
                                'Sens et motivation': saisines[i][8],
                                'Partie': saisines[i][9],
                                'Avis': saisines[i][10],
                                'Date reception': dateReception[saisines[i][0]],
                                'Delai': duration.asDays()
                            });

                            var type = saisines[i][8].split(",")[0].split("/")[0];
                            if (type == '') {
                                type = 'Sans objet';
                            }
                            if (!delaiParAnneeParType[saisines[i][3]]) {
                                delaiParAnneeParType[saisines[i][3]] = {
                                    'Favorable': [],
                                    'Défavorable': [],
                                    'Incompétence': [],
                                    'Irrecevable': [],
                                    'Sans objet': []
                                };
                            }
                            delaiParAnneeParType[saisines[i][3]][type].push(duration.asDays());
                        }
                    } else {
                        // text mining
                        var datesExtracted = extractDates(saisines[i]);
                        var dateSaisine = parseDate(datesExtracted[0]);
                        if (dateSaisine != undefined) {
                            var duration = moment.duration(dateAvis.diff(dateSaisine));
                            if (!duration.asDays() && datesExtracted[0] != saisines[i][4]) {
                                // error not found
                                //console.log(duration.asDays());
                                //console.log(datesExtracted);
                                //console.log(saisines[i][4]);
                                console.log("Erreur : Avis " + saisines[i][0] + "  (date non trouvée).");
                            } else if (duration.asDays() < 0) {
                                // error negative
                                console.log("Erreur : Avis " + saisines[i][0] + " rendu le " + saisines[i][4] + " alors que saisine le " + datesExtracted[0] + " (" + duration.asDays() + " jours) !");
                                incrementError(saisines[i][3]);
                            } else if (duration.asDays() == 0) {
                                // error 0 value
                                console.log("Erreur : Avis " + saisines[i][0] + "  (détection du même jour).");
                                incrementError(saisines[i][3]);
                            } else if (duration.asDays() > 365 * 2) {
                                // error too long
                                /*console.log(duration.asDays());
                                console.log(datesExtracted);
                                console.log(saisines[i]);*/
                                console.log("Erreur : Avis " + saisines[i][0] + "  (date > 2 ans).");
                                incrementError(saisines[i][3]);
                            } else if (duration.asDays() < 3) {
                                // error too short
                                //console.log(duration.asDays());
                                //console.log(datesExtracted);
                                //console.log(saisines[i]);
                                console.log("Erreur : Avis " + saisines[i][0] + "  (date < 3 jours).");
                                incrementError(saisines[i][3]);
                            } else {
                                if (!durationsPerYear[saisines[i][3]]) {
                                    durationsPerYear[saisines[i][3]] = [];
                                }
                                durationsPerYear[saisines[i][3]].push(duration.asDays())
                                durations.push(duration.asDays());
                            }
                        } else {
                            console.log("Erreur : Aucune date de saisine extraite !");
                            statsError++;
                        }
                    }

                }
                console.log("Résumé des statistiques par années (année, min, moyenne, max) en jours :");
                for (y in durationsPerYear) {
                    //console.log("Stats " + y + " :");
                    var s = stats(durationsPerYear[y]);
                    console.log(y + "," + Math.round(s.min) + "," + Math.round(s.mean) + "," + Math.round(s.max));
                }
                console.log("Délai par année par type d'avis (année, 'Favorable', 'Défavorable', 'Incompétence', 'Irrecevable', 'Sans objet')");
                for (y in delaiParAnneeParType) {
                    console.log(y + "," + arrayAverage(delaiParAnneeParType[y]['Favorable']) + "," + arrayAverage(delaiParAnneeParType[y]['Défavorable']) + "," + arrayAverage(delaiParAnneeParType[y]['Incompétence']) + "," + arrayAverage(delaiParAnneeParType[y]['Irrecevable']) + "," + arrayAverage(delaiParAnneeParType[y]['Sans objet']));
                }
                console.log("TOP 10 les plus longues");
                var top10 = saisinesAvecDelai.sort((a, b) => b['Delai'] - a['Delai']).slice(0, 10);
                console.log(top10);
                console.log("--")
                for (var top in top10) {
                    console.log('<li>' + top10[top]['Administration'] + ' : <a href="https://cada.data.gouv.fr/' + top10[top]['Numéro de dossier'] + '/" target="_blank">' + top10[top]['Objet'].replace(/(\r\n)+|\r+|\n+|\t+/gm, '<br/>') + '</a> ' + moment(top10[top]['Date reception']).format('DD/MM/YYYY') + ' ➜ ' + parseDate(top10[top]['Séance']).format('DD/MM/YYYY') + ' (' + Math.round(top10[top]['Delai']) + ' jours soit plus de '+Math.trunc(moment.duration(top10[top]['Delai'], 'days').asMonths())+' mois)</li>');
                }
                console.log("--")
                console.log("TOP 10 les plus courtes");
                var top10 = saisinesAvecDelai.sort((a, b) => a['Delai'] - b['Delai']).slice(0, 10);
                console.log(top10);
                console.log("--")
                for (var top in top10) {
                    console.log('<li>' + top10[top]['Administration'] + ' : <a href="https://cada.data.gouv.fr/' + top10[top]['Numéro de dossier'] + '/" target="_blank">' + top10[top]['Objet'].replace(/(\r\n)+|\r+|\n+|\t+/gm, '<br/>') + '</a> ' + moment(top10[top]['Date reception']).format('DD/MM/YYYY') + ' ➜ ' + parseDate(top10[top]['Séance']).format('DD/MM/YYYY') + ' (' + Math.round(top10[top]['Delai']) + ' jours)</li>');
                }
                console.log("--")
                console.log("Total par année :");
                console.log(totalPerYear);
                console.log("Détail des type de réponse par année :");
                for(var annee in saisinesParAnneeParType){
                    console.log(annee+", "+saisinesParAnneeParType[annee]["Favorable"]+", "+saisinesParAnneeParType[annee]["Défavorable"]+", "+saisinesParAnneeParType[annee]["Incompétence"]+", "+saisinesParAnneeParType[annee]["Irrecevable"]+", "+saisinesParAnneeParType[annee]["Sans objet"]);
                }
                console.log("Stats globales :");
                console.log(stats(durations));
                console.log(statsError + " erreurs (date non détectée). Détail par année :");
                console.log(errorsPerYear);
                console.log("Average length per year");
                for(var y in lengthPerYear){
                    console.log(y+","+arrayAverage(lengthPerYear[y]));
                }
            });

    });



var datesExceptions = [
	'17/07/1978', // "article 6 de la loi du 17 juillet 1978"
	'06/01/1978',
	'01/07/1901'
];

function extractDates(saisine) {
    var results = [];
    var texte = saisine[10].replace("  ", " ");
    texte = texte.slice(0, 200);

    var mois = [' janvier', ' février', ' mars', ' avril', ' mai', ' juin', ' juillet', ' août', ' septembre', ' octobre', ' novembre', ' décembre']
    var search = / janvier| février| mars| avril| mai| juin| juillet| août| septembre| octobre| novembre| décembre/gm;

    while ((result = search.exec(texte)) !== null) {
        if (parseInt(result.input.slice(result.index - 2, result.index)) || result.input.slice(result.index - 3, result.index) == "1er") {
            if (result.input.slice(result.index - 3, result.index) == "1er") {
                var dateComplete = result.input.slice(result.index, result.index + result[0].length + 5);
                dateComplete = "01" + dateComplete;
            } else {
                var dateComplete = result.input.slice(result.index - 2, result.index + result[0].length + 5);
            }
            if (dateComplete[0] == " ") {
                dateComplete = "0" + dateComplete.slice(1);
            }

            var moisChiffre = "" + (mois.findIndex(mois => mois === result[0]) + 1);
            if (moisChiffre.length < 2) {
                moisChiffre = "0" + moisChiffre;
            }
            var dateFormat = dateComplete.replace(result[0], "/" + moisChiffre);
            dateFormat = dateFormat.replace(" ", "/");
            dateFormat = dateFormat.replace(",", "/");

            if (!parseInt(dateFormat.split("/")[2]) || parseInt(dateFormat.split("/")[2]) < 1000) {
                dateFormat = dateFormat.split("/")[0] + "/" + dateFormat.split("/")[1] + "/" + saisine[3];
            }

            if (dateFormat == saisine[4] || !datesExceptions.includes(dateFormat)) {
                results.push(dateFormat);
            }
        }
    }
    return results;
}

function parseDate(date) {
    if (date) {
        var parts = date.split("/");
        return moment(new Date(parts[2], parts[1], parts[0]));
    }
}

function stats(arr) {
    var sum = 0;
    var total = 0;
    var min = 99999;
    var max = -1;
    for (var i = 0; i < arr.length; i++) {
        sum += arr[i];
        total++;
        if (arr[i] < min) {
            min = arr[i];
        }
        if (arr[i] > max) {
            max = arr[i];
        }
    }
    return {
        "total": total,
        "min": min,
        "mean": sum / total,
        "max": max
    };
}

function arrayAverage(arr) {
    var sum = 0;
    for (var i in arr) {
        sum += arr[i];
    }
    return Math.round(sum / arr.length);
}

function incrementError(year) {
    statsError++;
    if (errorsPerYear[year]) {
        errorsPerYear[year]++;
    } else {
        errorsPerYear[year] = 1;
    }
}